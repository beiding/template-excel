# template-excel

#### 介绍
本组件是基于poi封装的快速渲染Excel以及从Excel反向提取数据的功能组件.组件将繁琐的实体映射Excel单元格的过程进行简化,实现通过模板将数据实体渲染至Excel,也可通过模板将Excel提取为数据实体操作.组件支持在渲染表达式中使用函数,进行反转/填充/替换等操作,也可以使用com.gitee.beiding.template_excel.Js类自定义函数为组件提供更丰富的功能.


#### 安装教程
    <dependency>
        <groupId>com.gitee.beiding</groupId>
        <artifactId>template-excel</artifactId>
        <version>2.0.1-RELEASE</version>
    </dependency>

#### 使用说明

请参考https://gitee.com/beiding/template-excel-demo