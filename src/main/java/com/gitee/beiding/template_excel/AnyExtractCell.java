package com.gitee.beiding.template_excel;

import java.util.HashMap;
import java.util.Map;

class AnyExtractCell extends ExtractCell {

    static AnyExtractCell compile(String exp) {
        return ExtractCellCompileUtils.handle(exp);
    }


    AnyExtractCell() {
    }

    String name;

    public Map<String, ValueHolder> extract(Object object, Merge merge) {

        //默认只要一行满足
        int rowNumber = 1;

        if (this.merge != merge) {

            //如果列不相同直接返回不匹配
            if (this.merge.getCol() != merge.getCol()) {
                return null;
            }

            if (this.merge.getRow() == 1) {
                rowNumber = merge.getRow();
            } else {
                return null;
            }
        }

        Map<String, ValueHolder> map = new HashMap<>();
        map.put(name, new ValueHolder(object, rowNumber));
        this.blankToken = rowNumber - 1;
        return map;
    }

}
