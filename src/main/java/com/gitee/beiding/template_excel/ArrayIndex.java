package com.gitee.beiding.template_excel;

import java.util.*;
import java.util.stream.Collectors;

class ArrayIndex {

    private W last;


    /*
     */
    private Map<String, LinkedHashSet<String>> checkJsMap;

    //在初始化以后就使用执行一遍检查
    void init(LinkedHashMap<String, LinkedHashSet<String>> checkJsMap) {
        this.checkJsMap = checkJsMap;
        List<String> list = checkJsMap.keySet().stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        W f = last = new W();
        f.name = list.get(0);

        //最低位从-1开始
        last.value = -1;

        Js.set(last.name, last.value);

        for (int i = 1; i < list.size(); i++) {//构建链结构
            W w = new W();
            w.name = list.get(i);
            w.next = f;
            f.pre = w;
            f = w;
            Js.set(f.name, f.value);
        }
    }

    //从最后一位开始自增
    boolean autoIncrement() {
        return autoIncrement(last);
    }


    //当前自增是否有效
    private boolean autoIncrement(W current) {

        try {

            boolean flag = true;

            //同步到js
            current.value++;
            Js.set(current.name, current.value);

            //执行检查脚本
            Set<String> set = checkJsMap.get(current.name);
          //  set.addAll(cj);

            for (String js : set) {

                try {
                    //强转为Boolean型
                    if ((Boolean) Js.exe(js)) {
                        flag = false;
                        break;
                    }
                } catch (Exception e) {

                    flag = false;
                    break;

                }
            }



            if (flag) {


                return true;
            } else {//自增已到达数当前位边界

                //已经到最大
                if (current.pre == null) {

                    return false;
                } else {
                    //重置当前位
                    current.value = 0;
                    Js.set(current.name, current.value);

                    //上一位自增
                    return autoIncrement(current.pre);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    //清空所有有关的局部变量
    void clear() {
        for (String s : checkJsMap.keySet()) {
            Js.remove(s);
        }
    }

    //位
    private class W {

        //名称
        String name;

        int value = 0;

        //下一位
        W next;

        //上一位
        W pre;

    }


}
