package com.gitee.beiding.template_excel;

/**
 * @author 丁常磊
 * @date 2021/5/27 9:15
 */

public class AutoWidthValue {

    private Object value;

    //留白
    private int blankSpace = 0;

    public void setValue(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public void setBlankSpace(int blankSpace) {
        this.blankSpace = blankSpace;
    }

    public int getBlankSpace() {
        return blankSpace;
    }
}
