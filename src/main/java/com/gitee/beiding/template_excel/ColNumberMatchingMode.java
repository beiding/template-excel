package com.gitee.beiding.template_excel;

public enum  ColNumberMatchingMode {
    /*
      可以指定列的匹配模式
          匹配模式有三种:
              1. ColNumberMatchingMode.EQUALS:全等匹配
                   这种模式要求数据行中的数据必须严格匹配模板行中提取表达式
                   例如:
                      数据行:    |           |  你      |好      |
                      模板行1:   |           |${/你/}   |${/好/} |
                      模板行2:   |           |${/你/}   |        |
                      模板行3:   |${/其他/}   |${/你/}   |        |

                   数据行可以匹配模板行1,无法匹配模板行2和模板行3

              2.ColNumberMatchingMode.TEMPLATE_FULL:模板行全匹配模式
                   这种模式要求数据行中的数据能够完全满足模板行中的提取表达式
                   例如:在上例中数据行可以满足模板行1和模板行2,但是无法满足模板行3

              3.ColNumberMatchingMode.INTERSECTION:交集匹配
                  这种模式下只要数据行和模板行至少存在一个交集即可
                  例如:在上例中数据行满足模板行1、模板行2以及模板行3

   */
    EQUALS,INTERSECTION, TEMPLATE_FULL,
}
