package com.gitee.beiding.template_excel;


import com.alibaba.fastjson.JSONObject;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * 废弃
 */
public class Config {

    /**
     * 日期解析格式化
     */
    private static DateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void setDataFormat(String format) {
        dataFormat = new SimpleDateFormat(format);
    }

    public static DateFormat getDataFormat() {
        return dataFormat;
    }

    //js工作器数量
    private static int coreJsWorkerNumber = 8;

    public static void setCoreJsWorkerNumber(int coreJsWorkerNumber) {
        Config.coreJsWorkerNumber = coreJsWorkerNumber;
    }

    public static int getCoreJsWorkerNumber() {
        return coreJsWorkerNumber;
    }


    public static void main(String[] args) throws IOException {

        String txt = "{\n" +
                "  \"list\": [\n" +
                "    {\n" +
                "      \"ksjd\": [\n" +
                "        {\n" +
                "          \"subjects\": [\n" +
                "            {\n" +
                "              \"name\": \"语文\",\n" +
                "              \"value\": \"\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"name\": \"数学\",\n" +
                "              \"value\": \"\"\n" +
                "            }\n" +
                "          ],\n" +
                "          \"name\": \"初试\"\n" +
                "        },\n" +
                "        {\n" +
                "          \"subjects\": [\n" +
                "            {\n" +
                "              \"name\": \"英语\",\n" +
                "              \"value\": \"\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"name\": \"政治\",\n" +
                "              \"value\": \"\"\n" +
                "            }\n" +
                "          ],\n" +
                "          \"name\": \"复试\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"xm\": \"李四\",\n" +
                "      \"zjlx\": \"01\",\n" +
                "      \"zjhm\": \"333\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        JSONObject data = JSONObject.parseObject(txt);

        System.out.println(txt);

        XSSFWorkbook render = Renderer.render(data,
                PoiUtils.read(new FileInputStream("C:\\Users\\dinglei\\Desktop\\报名系统\\examination\\src\\main\\resources\\excelteamplte\\stu\\成绩导入模板.xlsx"))
                , new RenderContext().autoMerge(true));

        PoiUtils.write(render, new File("doc/成绩导出验证.xlsx"));

        ExtractResult extract = Extractor.extract(PoiUtils.read(new FileInputStream("C:\\Users\\dinglei\\Desktop\\报名系统\\examination\\src\\main\\resources\\excelteamplte\\stu\\成绩导入模板.xlsx")),
                PoiUtils.read(new File("doc/成绩导出验证.xlsx")), null,
                new ExtractContext()
        );

        System.out.println(extract.getData());

    }
}
