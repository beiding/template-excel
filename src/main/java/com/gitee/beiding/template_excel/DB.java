package com.gitee.beiding.template_excel;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

class DB {

    //所有字段
    private Set<String> cols;

    //所有数据
    private Map<String, Object> map = new HashMap<>();

    //
    DB(Set<String> cols) {
        this.cols = cols;
    }

    //使用and条件查询结果
    RenderHolder andValue(TemplateCell templateCell, Map<String, Integer> condition) {
        String key = conditionToKey(condition);
        if (map.containsKey(key)) {
            Object o = map.get(key);
            return new RenderHolder(templateCell, key, o);
        }
        throw new InvalidIndexException();

    }

    void putValue(Map<String, Integer> condition, Object value) {
        map.put(conditionToKey(condition), value);
    }

    private String conditionToKey(Map<String, Integer> condition) {
        return cols.stream().map((k) -> k + "=" + condition.get(k) + ",").collect(Collectors.joining());
    }


}
