package com.gitee.beiding.template_excel;

import java.util.HashMap;
import java.util.Map;

public class Data {

    private Data() {
    }
    public static Data create() {
        return new Data();
    }
    private Map<String, Object> data = new HashMap<>();
    Map<String, Object> getData() {
        return data;
    }

    public Object set(String key, Object object) {
        return put(key, object);
    }
    public Object put(String key, Object object) {
        return data.put(key, object);
    }
    public Object remove(String key) {
        return data.remove(key);
    }
    public void putAll(Map<String, Object> map) {
        data.putAll(map);
    }

}
