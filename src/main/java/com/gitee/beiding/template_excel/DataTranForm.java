package com.gitee.beiding.template_excel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据转换
 * todo 导入导出时有必要再处理
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
public @interface DataTranForm {

    /**
     * 指定一个字段的转换器
     *
     * @return
     */
    Class<? extends TransForm> value();

}
