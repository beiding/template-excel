package com.gitee.beiding.template_excel;

public class ExtractContext {

    private ColNumberMatchingMode cellMatchingMode = ColNumberMatchingMode.EQUALS;

    public ExtractContext cellMatchingMode(ColNumberMatchingMode cellMatchingMode) {
        this.cellMatchingMode = cellMatchingMode;
        return this;
    }

    public ColNumberMatchingMode getCellMatchingMode() {
        return cellMatchingMode;
    }
}
