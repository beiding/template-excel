package com.gitee.beiding.template_excel;

import com.alibaba.fastjson.JSONObject;

public class ExtractResult {

    private JSONObject data;

    ExtractResult(JSONObject data) {
        this.data = data;
    }

    public JSONObject getData() {
        return data;
    }

}
