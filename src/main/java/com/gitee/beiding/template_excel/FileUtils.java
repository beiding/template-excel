package com.gitee.beiding.template_excel;

import java.io.*;

class FileUtils {

    static String read(InputStream inputStream) {
       try {
           StringBuilder builder = new StringBuilder();
           String line;
           BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
           while ((line = reader.readLine()) != null) {
               builder.append(line).append("\n");
           }
           reader.close();
           return builder.toString();
       } catch (Exception e) {
           return null;
       }
   }

    static String read(File file) {
       try {
           return read(new FileInputStream(file));
       } catch (Exception e) {
           return null;
       }
   }

    static void copy(InputStream inputStream, OutputStream outputStream) throws IOException {
       byte[] buff = new byte[1024];
       int len;
       while ((len = inputStream.read(buff)) != -1) {
           outputStream.write(buff, 0, len);
       }
   }

    static void copy(File src, File target) throws IOException {
       FileInputStream inputStream = new FileInputStream(src);
       FileOutputStream outputStream = new FileOutputStream(target);
       copy(inputStream, outputStream);
       inputStream.close();
       outputStream.close();
   }

}
