package com.gitee.beiding.template_excel;

import javax.script.Bindings;
import javax.script.SimpleBindings;

/**
 * 渲染上下文
 *
 */
public class RenderContext {

    private static final ThreadLocal<RenderContext> TL = new ThreadLocal<>();

    /**
     * 初始化
     */
    static void setCurrent(RenderContext renderContext) {
        TL.set(renderContext);
    }

    /**
     * 清理
     */
    static void clear() {
        TL.remove();
    }

    static RenderContext getCurrent() {
        return TL.get();
    }

    /**
     * 获取js绑定
     *
     * @return
     */
    Bindings getJsBinds() {
        return bindings;
    }

    public boolean isAutoMerge() {
        return getCurrent().autoMerge;
    }

    public RenderContext autoMerge(boolean autoMerge) {
        this.autoMerge = autoMerge;
        return this;
    }

    boolean initialized() {
        return this.initialized;
    }

    void setInitialized() {
        this.initialized = true;
    }

    private boolean initialized = false;

    private Bindings bindings = new SimpleBindings();

    private boolean autoMerge = true;

    public RenderContext() {

    }
}
