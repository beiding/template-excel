package com.gitee.beiding.template_excel;

/**
 * 渲染结果包裹
 * 可用于单元格合并
 *
 * @author 丁常磊
 * @date 2021/5/22 9:29
 */

public class RenderHolder {

    private Object value;

    private TemplateCell cell;

    private String key;

    public RenderHolder(TemplateCell cell, String key, Object value) {
        this.cell = cell;
        this.key = key;
        this.value = value;
    }
    public boolean eq(RenderHolder holder) {
        if (holder == null) {
            return false;
        }
        boolean b = cell == holder.cell && key.equals(holder.key);
        if (!b) {
            return false;
        }
        if (value == null) {
            return holder.value == null;
        }
        return value.equals(holder.value);
    }

    public Object getValue() {
        return value;
    }

}
