package com.gitee.beiding.template_excel;

public interface TransForm<F,T> {

    T trans(F v);

}
