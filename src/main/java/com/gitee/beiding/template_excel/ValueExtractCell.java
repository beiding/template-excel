package com.gitee.beiding.template_excel;

import java.util.Map;

class ValueExtractCell extends ExtractCell {

   private Object object;

    ValueExtractCell(Object object) {
       this.object = object;
   }

   @Override
   public Map<String, ValueHolder> extract(Object obj, Merge merge) {

       if (obj == null) {
           return null;
       }

       if (obj.equals(object)) {

           if (merge == this.merge) {
               return emptyMap;

           } else {

               if (this.merge.getCol() != merge.getCol()) {
                   return null;
               }

               if (this.merge.getRow() == 1) {
                   this.blankToken = merge.getRow() - 1;
                   return emptyMap;
               }

           }
       }
       return null;
   }

}
