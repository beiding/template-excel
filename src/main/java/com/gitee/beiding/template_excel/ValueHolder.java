package com.gitee.beiding.template_excel;

class ValueHolder {


    //值
    private Object value;

    //值的跨度
    private int rowSpan;

    ValueHolder(Object value, int row) {
        this.value = value;
        this.rowSpan = row;
    }


    void setValue(Object value) {
        this.value = value;
    }

    int getRowSpan() {
        return rowSpan;
    }

    Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value + "   " + rowSpan;
    }
}
