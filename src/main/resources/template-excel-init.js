var Picture = Java.type('com.gitee.beiding.template_excel.Picture');
var AutoWidthValue = Java.type('com.gitee.beiding.template_excel.AutoWidthValue');
var System = Java.type('java.lang.System');

function log(a) {
    System.out.println(a);
}

//拼接字符串
function concat() {
    if (arguments.length === 0) {
        return null;
    }
    if (arguments.length === 1) {
        return arguments[0];
    }
    var flag = true;
    var s = "";
    for (var i = 0; i < arguments.length; i++) {
        if (arguments[i]) {
            s += arguments[i];
            flag = false;
        }
    }
    if (flag) {
        return null;
    } else {
        return s;
    }
}

//cas方法
function cas(obj, from, to) {
    if (obj === from) {
        return to;
    } else {
        return obj;
    }
}

//使用正则提取
function extract(obj, regex) {
    if (!obj) {
        return obj;
    }
    var reg = new RegExp(regex);
    var s = obj.toString();
    var list = s.match(reg);
    if (list && list.length > 0) {
        return list[0];
    } else {
        return null;
    }
}

//寻找对象在列表中的位置
function indexOf(arr, obj) {
    return arr.indexOf(obj);
}


//单行扩展开的索引 从n开始
function $index(n) {
    return _java.createIndex(n, _index);
}

//全局索引 从n开始
function $globalIndex(n) {
    return _java.createIndex(n, _globalIndex);
}

//将数值原样输出
function $value(n) {
    return n;
}

//将数值原样输出
function $v(n) {
    return n;
}

function _clearCache() {
    _java.clearCache();
}

//放入缓存数据,内部方法
function _cachePut(args, result) {
    if (args.length === 0) {
        _java.blankArgCacheMap[args.callee.name] = result;
        return result
    }
    if (args.length === 1) {
        var map = _java.oneArgCacheMap[args.callee.name];
        if (map == null) {
            map = {};
            _java.oneArgCacheMap[args.callee.name] = map;
        }
        map[args[0]] = result;
        return result;
    }
    var arg = [];
    for (var i = 0; i < args.callee.length; i++) {
        arg[i] = args[i];
    }
    _java.cachePut(args.callee.name, arg, result);
    return result
}

//获取缓存数据,内部方法
function _cacheGet(args) {
    if (args.length === 0) {
        return _java.blankArgCacheMap[args.callee.name]
    }
    if (args.length === 1) {
        var map = _java.oneArgCacheMap[args.callee.name];
        if (map == null) {
            return null;
        }
        return map[args[0]];
    }
    var arg = [];
    for (var i = 0; i < args.callee.length; i++) {
        arg[i] = args[i];
    }
    return _java.cacheGet(args.callee.name, arg);
}

//反转数组
function reversal(list) {
    var r = _cacheGet(arguments);
    if (r != null) {
        return r;
    }
    if (list == null) {
        return null;
    }
    r = [];
    for (var i = 0; i < list.length; i++) {
        r[i] = list[list.length - 1 - i];
    }
    return _cachePut(arguments, r);
}

//生成一个数组
function array(len, value) {
    var r = _cacheGet(arguments);
    if (r != null) {
        return r;
    }
    r = [];
    if (!len) {
        return r;
    }
    if (value == null) {
        value = '';
    }
    for (var i = 0; i < len; i++) {
        r[i] = value;
    }
    return _cachePut(arguments, r);
}

//对数组赋值
function align(list, value) {
    var r = _cacheGet(arguments);
    if (r != null) {
        return r;
    }
    if (list == null) {
        return null;
    }
    if (value == null) {
        value = ""
    }
    r = [];
    for (var i = 0; i < list.length; i++) {
        r[i] = value;
    }
    return _cachePut(arguments, r);
}

//对数组填充
function fill(arr, value) {
    var r = _cacheGet(arguments);
    if (r != null) {
        return r;
    }
    if (arr == null) {
        return arr;
    }
    if (value == null) {
        value = '';
    }
    for (var i = 0; i < arr.length; i++) {
        if (!arr[i]) {
            arr[i] = value;
        }
    }
    return _cachePut(arguments, arr);
}

//修改数组
function change(arr, from, to) {
    var r = _cacheGet(arguments);
    if (r != null) {
        return r;
    }
    if (arr == null) {
        return arr;
    }
    if (from == null) {
        return arr;
    }
    if (to == null) {
        to = '';
    }
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === from) {
            arr[i] = to;
        }
    }
    return _cachePut(arguments, arr);
}

//创建图片对象
function picture(data) {
    var p = new Picture();
    p.setData(data);
    return p;
}

function $picture(data) {
    var p = new Picture();
    p.setData(data);
    return p;
}


function autoWidth(v, bs) {
    var r = new AutoWidthValue();
    r.value = v;
    if (typeof bs === 'number') {
        r.blankSpace = bs;
    }
    return r;
}